=== Export Simple 301 Redirects to CSV ===
Contributors: noblestudios, sterlo
Tags: redirects, export, csv
Requires at least: 4.3.1
Tested up to: 4.3.1
Stable tag: trunk
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Exports the contents of Simple 301 Redirects plugin to a CSV file.

== Description ==

A very simple plugin that allows you to export the contents of [Simple 301 Redirects](https://wordpress.org/plugins/simple-301-redirects/ "Simple 301 Redirects") plugin to a CSV file.

== Installation ==

1. Upload the plugin files to the `/wp-content/plugins/export-simple-301-redirects-to-csv` directory, or install the plugin through the WordPress plugins screen directly.
1. Activate the plugin through the 'Plugins' screen in WordPress
1. The export can only be ran via the 'Plugins' screen, there's a link next to the Deactivate link that says 'Export';
1. Super important note: This plugin will do absolutely nothing if you do not have the [Simple 301 Redirects](https://wordpress.org/plugins/simple-301-redirects/ "Simple 301 Redirects") plugin installed.

== Changelog ==

= 1.0 =
* Inception.

== GIT Host ==
A GIT repository of this plugin is available [here](https://gitlab.com/noblestudios/wordpress-plugin-export-simple-301-redirects-to-csv "noblestudios / wordpress-plugin-export-simple-301-redirects-to-csv").
