# Export Simple 301 Redirects to CSV

A very simple plugin that allows you to export the contents of [Simple 301 Redirects](https://wordpress.org/plugins/simple-301-redirects/) plugin to a CSV file.

Built by the team over at [Noble Studios](http://noblestudios.com).

If you're looking to do some development, head on over to the [contribution document](CONTRIBUTING.md).
