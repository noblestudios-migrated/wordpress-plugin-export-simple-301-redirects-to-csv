<?php
/*
 * Plugin Name: Export Simple 301 Redirects to CSV
 * Description: Exports the contents of Simple 301 Redirects plugin to a CSV file.
 * Version: 1.0
 * Author: Noble Studios
 * Author URI: http://noblestudios.com/
 * Stable tag: trunk
 * License: GPLv2 or later
 * License URI: http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This plugin, like WordPress, is licensed under the GPL.
 * Use it to make something cool, have fun, and share what you've learned with others.
 */

namespace WordPress\Plugins\NobleStudios\ExportSimpe301Redirects;

$namespace = 'WordPress\Plugins\NobleStudios\ExportSimpe301Redirects\\';

add_filter( 'plugin_action_links', $namespace . 'link', 10, 5  );
add_action( 'admin_init', $namespace . 'maybe_export' );

function link ( $actions, $plugin_file ) {
  static $plugin;
  if ( ! isset( $plugin ) ) {
    $plugin = plugin_basename( __FILE__ );
  }

  if ( $plugin == $plugin_file ) {
    $export = array( 'settings' => '<a target="blank" href="'. esc_url( get_admin_url( null, '?export-simple-301-redirects=true' ) ) .'">Export</a>' );
    $actions = array_merge( $export, $actions );
  }

  return $actions;
}

// Actually create the CSV file.
function export () {
  header( 'Content-Type: text/csv; charset=utf-8' );
  header( 'Content-Disposition: attachment; filename=simple-301-redirects.csv' );

  // This is the method of redirection rule retreival as of Version: 1.06
  // Source: http://plugins.svn.wordpress.org/simple-301-redirects/tags/1.06/wp-simple-301-redirects.php
  $redirects = get_option( '301_redirects' );

  if( $redirects ) {
    // In order to get the browser to auto download, we need to write to a file.
    // Open a file pointer, then fill it, then close it.
    $file_pointer = fopen( 'php://output', 'w' );
    foreach ( $redirects as $source => $destination ) {
        fputcsv( $file_pointer, array( $source, $destination ) );
    }
    fclose( $file_pointer );
  }
}

// In theory, the only way this query variable is set is if you click the plugin link.
// The function name is an homage to the WordPress core developers and "maybe_serialize".
function maybe_export () {
  if( $_GET[ 'export-simple-301-redirects' ] === 'true' ) {
    export();
    exit;
  }
}
