# Contributing

You should fork, then do work, then submit a pull request.

## Installation

1. Upload the plugin files to the `/wp-content/plugins/export-simple-301-redirects-to-csv` directory, or install the plugin through the WordPress plugins screen directly.
2. Activate the plugin through the 'Plugins' screen in WordPress
3. The export can only be ran via the 'Plugins' screen, there's a link next to the Deactivate link that says 'Export';
4. Super important note: This plugin will do absolutely nothing if you do not have the [simple-301-redirects](https://wordpress.org/plugins/simple-301-redirects/) plugin installed.

## Support

Please use the [WordPress Support](http://wordpress.org/support/plugin/export-simple-301-redirects-to-csv) page to submit requests for help or bugs.
